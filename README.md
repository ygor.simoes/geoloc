# Demonstração - Spoof Geolocation

Ferramentas utilizadas: 
- [Spoof Geolocation Extension (Firefox)](https://addons.mozilla.org/en-US/firefox/addon/spoof-geolocation/)
- [Anonsurf](https://github.com/Und3rf10w/kali-anonsurf)
- [Script Geoloc (Golang)](https://gitlab.com/ygor.simoes/geoloc/-/blob/main/geoloc.go)
