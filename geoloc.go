package main

import (
        "encoding/json"
        "fmt"
        "io/ioutil"
        "log"
        "net/http"
)

// Definindo a estrutura para a geolocalização
type GeoLocation struct {
        Query      string  `json:"query"`
        City       string  `json:"city"`
        Region     string  `json:"region"`
        Country    string  `json:"country"`
        Lat        float64 `json:"lat"`
        Lon        float64 `json:"lon"`
}

func getIPGeoLocation() (*GeoLocation, error) {
        response, err := http.Get("http://ip-api.com/json/")
        if err != nil {
                return nil, err
        }
        defer response.Body.Close()

        body, _ := ioutil.ReadAll(response.Body)

        var geo GeoLocation
        err = json.Unmarshal(body, &geo)
        if err != nil {
                return nil, err
        }

        return &geo, nil
}

func main() {
        geo, err := getIPGeoLocation()
        if err != nil {
                log.Fatal(err)
        }

        fmt.Printf("IP: %s\n", geo.Query)
        fmt.Printf("City: %s\n", geo.City)
        fmt.Printf("Region: %s\n", geo.Region)
        fmt.Printf("Country: %s\n", geo.Country)
        fmt.Printf("Latitude: %f\n", geo.Lat)
        fmt.Printf("Longitude: %f\n", geo.Lon)
}
